EESchema Schematic File Version 4
LIBS:RPi_Zero_pHat_Template-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector-ML:RPi_GPIO J1
U 1 1 5515D39E
P 850 800
F 0 "J1" H 1600 1050 60  0000 C CNN
F 1 "RPi_GPIO" H 1600 950 60  0000 C CNN
F 2 "RPi_Hat:Samtec_HLE-120-02-XXX-DV-BE-XX-XX" H 850 800 60  0001 C CNN
F 3 "" H 850 800 60  0000 C CNN
	1    850  800 
	1    0    0    -1  
$EndComp
$Comp
L Connector-ML:RPi_GPIO J2
U 1 1 5516AE26
P 8500 850
F 0 "J2" H 9250 1100 60  0000 C CNN
F 1 "RPi_GPIO" H 9250 1000 60  0000 C CNN
F 2 "RPi_Hat:Pin_Header_Straight_2x20" H 8500 850 60  0001 C CNN
F 3 "" H 8500 850 60  0000 C CNN
	1    8500 850 
	1    0    0    -1  
$EndComp
Text Notes 650  3100 0    100  Italic 0
Surface Mount Connector
Text Notes 8450 3150 0    100  Italic 0
Thru-Hole Connector
Text Notes 650  3300 0    100  Italic 0
Socket on bottom
$Sheet
S 3400 700  4100 2400
U 60BB2F01
F0 "Sheet60BB2F00" 50
F1 "Power_Supply_Circuit.sch" 50
$EndSheet
Wire Wire Line
	7350 1350 8050 1350
Wire Wire Line
	8300 1450 8050 1450
Text Label 7350 1350 0    50   ~ 0
GPIO17
Text Label 7350 1450 0    50   ~ 0
GPIO27
$Sheet
S 900  3700 5150 3050
U 60BB3AAF
F0 "Sheet60BB3AAE" 50
F1 "Amplifier.sch" 50
$EndSheet
Connection ~ 8050 1450
Wire Wire Line
	8050 1450 7350 1450
Connection ~ 8050 1350
Wire Wire Line
	8050 1350 8300 1350
Wire Wire Line
	8050 1450 8050 1350
Wire Wire Line
	8300 1250 8150 1250
Text Label 8150 1250 0    50   ~ 0
GND
$Sheet
S 6500 3650 4500 2650
U 60BB44E2
F0 "Sheet60BB44E1" 50
F1 "LEDs.sch" 50
$EndSheet
Wire Wire Line
	8300 850  8050 850 
Wire Wire Line
	8050 850  8050 1350
Text Label 8100 2250 0    50   ~ 0
GPIO05
Wire Wire Line
	8050 1450 8050 2250
Wire Wire Line
	8050 2250 8300 2250
Wire Wire Line
	8300 2750 8050 2750
Wire Wire Line
	8300 2050 8100 2050
Wire Wire Line
	10200 1450 10400 1450
Wire Wire Line
	10200 1050 10400 1050
Wire Wire Line
	10200 1750 10400 1750
Wire Wire Line
	10200 2250 10400 2250
Wire Wire Line
	10200 2450 10400 2450
Text Label 8100 2050 0    50   ~ 0
GND
Text Label 8050 2750 0    50   ~ 0
GND
Text Label 10400 1050 0    50   ~ 0
GND
Text Label 10400 1450 0    50   ~ 0
GND
Text Label 10400 1750 0    50   ~ 0
GND
Text Label 10400 2250 0    50   ~ 0
GND
Text Label 10400 2450 0    50   ~ 0
GND
Wire Wire Line
	10200 1350 10700 1350
Wire Wire Line
	10200 1550 10700 1550
Wire Wire Line
	10200 1650 10700 1650
Text Label 10800 1350 0    50   ~ 0
GPIO18
Text Label 10800 1550 0    50   ~ 0
GPIO23
Text Label 10800 1650 0    50   ~ 0
GPIO24
Wire Wire Line
	10200 850  10700 850 
Connection ~ 10700 1650
Wire Wire Line
	10700 1650 10800 1650
Wire Wire Line
	10700 850  10700 1350
Connection ~ 10700 1350
Wire Wire Line
	10700 1350 10800 1350
Wire Wire Line
	10700 1350 10700 1550
Connection ~ 10700 1550
Wire Wire Line
	10700 1550 10800 1550
Wire Wire Line
	10700 1550 10700 1650
NoConn ~ 10200 950 
NoConn ~ 10200 1150
NoConn ~ 10200 1250
NoConn ~ 10200 1850
NoConn ~ 10200 1950
NoConn ~ 10200 2050
NoConn ~ 10200 2150
NoConn ~ 10200 2350
NoConn ~ 10200 2550
NoConn ~ 10200 2650
NoConn ~ 10200 2750
NoConn ~ 8300 2650
NoConn ~ 8300 2550
NoConn ~ 8300 2450
NoConn ~ 8300 2350
NoConn ~ 8300 2150
NoConn ~ 8300 1950
NoConn ~ 8300 1850
NoConn ~ 8300 1750
NoConn ~ 8300 1650
NoConn ~ 8300 1550
NoConn ~ 8300 1150
NoConn ~ 8300 1050
NoConn ~ 8300 950 
NoConn ~ 650  800 
NoConn ~ 650  900 
NoConn ~ 650  1000
NoConn ~ 650  1100
NoConn ~ 650  1200
NoConn ~ 650  1300
NoConn ~ 650  1400
NoConn ~ 650  1500
NoConn ~ 650  1600
NoConn ~ 650  1700
NoConn ~ 650  1800
NoConn ~ 650  1900
NoConn ~ 650  2000
NoConn ~ 650  2100
NoConn ~ 650  2200
NoConn ~ 650  2300
NoConn ~ 650  2400
NoConn ~ 650  2500
NoConn ~ 650  2600
NoConn ~ 650  2700
NoConn ~ 2550 2700
NoConn ~ 2550 2600
NoConn ~ 2550 2500
NoConn ~ 2550 2400
NoConn ~ 2550 2300
NoConn ~ 2550 2200
NoConn ~ 2550 2100
NoConn ~ 2550 2000
NoConn ~ 2550 1900
NoConn ~ 2550 1800
NoConn ~ 2550 1700
NoConn ~ 2550 1600
NoConn ~ 2550 1500
NoConn ~ 2550 1400
NoConn ~ 2550 1300
NoConn ~ 2550 1200
NoConn ~ 2550 1100
NoConn ~ 2550 1000
NoConn ~ 2550 900 
NoConn ~ 2550 800 
$EndSCHEMATC
